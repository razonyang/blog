---
layout: post
title:  "PHP 扩展开发之快速入门"
categories: [php, php-extension]
tags: [php, php-extension]
permalink: /php-extension/quick-start.html
comments: true
---

本文讲

```
```


```
[Desktop Entry]                                                       //每个desktop文件都以这个标签开始，说明这是一个Desktop Entry文件
Version = 1.0                                                           //标明Desktop Entry的版本（可选）
Name = xampp                                                      //程序名称（必须），这里以创建一个xampp的快捷方式为例
GenericName = xampp                                        //程序通用名称（可选）
Comment = xampp                                               //程序描述（可选）
Exec = /opt/lampp/manager-linux-x64.run        //程序的启动命令（必选），可以带参数运行
                                                                                  //当下面的Type为Application，此项有效
Icon = /opt/lampp/htdocs/favicon.ico                 //设置快捷方式的图标（可选）
Terminal = false                                                    //是否在终端中运行（可选），当Type为Application，此项有效
Type = Application                                                 //desktop的类型（必选），常见值有“Application”和“Link”
Categories = GNOME;Application;Network;    //注明在菜单栏中显示的类别（可选）
```

