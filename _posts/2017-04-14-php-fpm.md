---
layout: post
title:  "PHP-FPM 的启动、关闭、重启命令"
categories: [php, php-fpm]
tags: [php, php-fpm]
permalink: /php/php-fpm.html
comments: true
---

假设 PHP 的安装目录为 `/usr/local/php`，那么 `PHP-FPM` 位置： `/usr/local/php/sbin/php-fpm`。

## PHP-FPM 启动

启动很简单，直接运行即可：

```
$ /usr/local/php/sbin/php-fpm
```

## PHP-FPM 关闭和重启

要关闭、重启 `PHP-FPM`，需要获取 `PHP-FPM` 的进程 ID（`PID`），然后利用 `kill` 命令向其发送相应的信号量：

- SIGINT - 中断信号，关闭 `PHP-FPM`
- SIGUSR2 - 用户自定义信号2，重启 `PHP-FPM`

### 获取 PHP-FPM 的进程 ID

获取 `PHP-FPM` 的 `PID` 方法有多种，可以通过 `ps` 和 `awk` 命令获取，也可以通过配置 `PHP-FPM` 的 `pid` 文件来获取。

#### 通过 `ps` 获取进程 ID

```
$ ps aux | grep php-fpm | grep master | awk '{print $2}'
29678
```

#### 通过 `PHP-FPM` 的 `pid` 文件获取进程 ID

```
$ sudo vi /usr/local/php/etc/php-fpm.conf
```

去掉 `;pid = run/php-fpm.pid` 的注释，然后保存，那么下次 `PHP-FPM` 启动后，会将其 PID 保存在此文件中。

```
$ cat /usr/local/php/var/run/php-fpm.pid
29678
```

#### 关闭 `PHP-FPM`

```
$ kill -INT 29678
```


#### 重启 `PHP-FPM`

```
$ kill -USR2 29678
```
